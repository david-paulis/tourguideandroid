package com.example.devinaboles.tourguide;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class MuseumsFragment extends Fragment {


    public MuseumsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.destinations_listview, container, false);
        final ArrayList<Destination> destinations = new ArrayList<>();
        destinations.add(new Destination(R.string.M_Coptic_title, R.string.M_Coptic_info, R.string.M_Coptic_history, new Location("cairo", 1, "deniel st")));
        destinations.add(new Destination(R.string.M_Egyptian_title, R.string.M_Egyptian_info, R.string.M_Egyptian_history, new Location("cairo", 5, "gork st")));
        destinations.add(new Destination(R.string.M_Abdeen_Palace_title, R.string.M_Abdeen_Palace_info, R.string.M_Abdeen_Palace_history, new Location("cairo", 2, "youue")));
        destinations.add(new Destination(R.string.M_Qasr_title, R.string.M_Qasr_info, R.string.M_Qasr_history, new Location("cairo", 8, "nour st")));
        destinations.add(new Destination(R.string.M_Ahmed_Shawki_title, R.string.M_Ahmed_Shawki_info, R.string.M_Ahmed_Shawki_history, new Location("cairo", 8, "nour st")));
        destinations.add(new Destination(R.string.M_Islamic_Art_title, R.string.M_Islamic_Art_info, R.string.M_Islamic_Art_history, new Location("cairo", 8, "nour st")));

        ListAdapter listAdapter = new ListAdapter(getActivity(), destinations);

        ListView listView = (ListView) rootView.findViewById(R.id.list);
        listView.setAdapter(listAdapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Destination destination = destinations.get(position);
                Intent intent = new Intent(getActivity(), DetailsActivity.class);
                intent.putExtra("title", destination.getTitle());
                intent.putExtra("info", destination.getInfo());
                intent.putExtra("history", destination.getHistory());
                intent.putExtra("location", destination.getLocation().toString());
                startActivity(intent);
            }
        });

        return rootView;
    }

}
