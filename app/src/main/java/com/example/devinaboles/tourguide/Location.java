package com.example.devinaboles.tourguide;

/**
 * Created by Devina Boles on 15/06/2017.
 */

public class Location {

    private String governorate;
    private int number;
    private String street;

    public Location(String governorate, int number, String street) {
        this.governorate = governorate;
        this.number = number;
        this.street = street;
    }

    public String getGovernorate() {
        return governorate;
    }

    public int getNumber() {
        return number;
    }

    public String getStreet() {
        return street;
    }

    @Override
    public String toString() {
        return "Governorate :" + governorate + "\n street :" + street + "\n number :" + number;
    }
}
