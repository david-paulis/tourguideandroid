package com.example.devinaboles.tourguide;

import android.content.Context;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by Devina Boles on 15/06/2017.
 */

public class ListAdapter extends ArrayAdapter<Destination> {

    public ListAdapter(Context context, ArrayList<Destination> Destinations) {
        super(context, 0, Destinations);
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        View destination = convertView;
        if (destination == null) {
            destination = LayoutInflater.from(getContext()).inflate(
                    R.layout.list_item, parent, false);
        }
        Destination item = getItem(position);

        TextView title = (TextView) destination.findViewById(R.id.destination_item);
        ImageView img = (ImageView) destination.findViewById(R.id.image);
        if (item.hasImage()) {
            img.setImageResource(item.getImgResourceId());
            // Make sure the view is visible
            img.setVisibility(View.VISIBLE);
        } else {
            img.setVisibility(View.GONE);
        }
        title.setText(item.getTitle());
        return destination;

    }


}
