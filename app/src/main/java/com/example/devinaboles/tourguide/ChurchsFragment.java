package com.example.devinaboles.tourguide;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;


public class ChurchsFragment extends Fragment {


    public ChurchsFragment() {
        // Required empty public constructor
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
       View rootView = inflater.inflate(R.layout.destinations_listview, container, false);
        final ArrayList<Destination> dest =new ArrayList<>();
        dest.add(new Destination(R.string.C_Barbara_Church_title,R.string.C_Barbara_Church_info,R.string.C_Barbara_Church_history,new Location("cairo",10,"zewala st")));
        dest.add(new Destination(R.string.C_Holy_Virgin_title,R.string.C_Holy_Virgin_info,R.string.C_Holy_Virgin_history,new Location("masr eladema",12,"zewala st")));
        dest.add(new Destination(R.string.C_Mary_Church_title,R.string.C_Mary_Church_info,R.string.C_Mary_Church_history,new Location("masr eladema",15,"zewala st")));
        dest.add(new Destination(R.string.C_Saint_Mercurius_title,R.string.C_Saint_Mercurius_info,R.string.C_Saint_Mercurius_history,new Location("gzerat eldahab",2,"saada st")));
        dest.add(new Destination(R.string.C_Mark_Coptic_Orthodox_Heliopolis_title,R.string.C_Mark_Coptic_Orthodox_Heliopolis_info,R.string.C_Mark_Coptic_Orthodox_Heliopolis_history,new Location("heliopolis",255,"masr")));
         ListAdapter listAdapter= new ListAdapter(getActivity(),dest);

        ListView listView = (ListView) rootView.findViewById(R.id.list);
        listView.setAdapter(listAdapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
               Destination destination= dest.get(position);
                Intent intent =new Intent(getActivity(),DetailsActivity.class);
                intent.putExtra("title",destination.getTitle());
                intent.putExtra("info",destination.getInfo());
                intent.putExtra("history",destination.getHistory());
                intent.putExtra("location",destination.getLocation().toString());

                if(destination.hasImage())
                {
                    intent.putExtra("hasImage",true);
                    intent.putExtra("imageId",destination.getImgResourceId());
                }else{
                    intent.putExtra("hasImage",false);
                }

                startActivity(intent);
            }
        });
          return  rootView;
    }

}
