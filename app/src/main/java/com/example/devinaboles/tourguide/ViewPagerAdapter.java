package com.example.devinaboles.tourguide;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

/**
 * Created by Devina Boles on 15/06/2017.
 */

public class ViewPagerAdapter extends FragmentPagerAdapter {

    private Context mContext;

    public ViewPagerAdapter(Context context, FragmentManager fm) {
        super(fm);
        mContext = context;
    }

    @Override
    public Fragment getItem(int position) {
        if (position == 0) {
            return new MuseumsFragment();
        } else if (position == 1) {
            return new HotelsFragment();
        } else if (position == 2) {
            return new ChurchsFragment();
        } else {
            return new MosqueFragment();
        }
    }

    @Override
    public int getCount() {
        return 4;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        if (position == 0) {
            return mContext.getString(R.string.museum_tab);
        } else if (position == 1) {
            return mContext.getString(R.string.hotel_tab);
        } else if (position == 2) {
            return mContext.getString(R.string.church_tab);
        } else {
            return mContext.getString(R.string.mosque_tab);
        }
    }
}
