package com.example.devinaboles.tourguide;

/**
 * Created by Devina Boles on 15/06/2017.
 */

public class Destination {
    private static final int No_IMAGE = -1;
    private int title;
    private int info;
    private int history;
    private Location location;
    private int imgResourceId = No_IMAGE;

    public Destination(int title, int info, int history, Location location, int imgResourceId) {
        this.title = title;
        this.info = info;
        this.history = history;
        this.location = location;
        this.imgResourceId = imgResourceId;
    }

    public Destination(int title, int info, int history, Location location) {
        this.title = title;

        this.info = info;
        this.history = history;
        this.location = location;
    }

    public int getTitle() {
        return title;
    }

    public int getInfo() {
        return info;
    }

    public int getHistory() {
        return history;
    }

    public Location getLocation() {
        return location;
    }

    public int getImgResourceId() {
        return imgResourceId;
    }

    public boolean hasImage() {
        return imgResourceId != No_IMAGE;
    }
}
