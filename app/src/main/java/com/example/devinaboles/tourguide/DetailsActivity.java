package com.example.devinaboles.tourguide;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import org.w3c.dom.Text;

public class DetailsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.detail);

        TextView title = (TextView) findViewById(R.id.title);
        TextView info = (TextView) findViewById(R.id.info);
        TextView history = (TextView) findViewById(R.id.history);
        TextView location = (TextView) findViewById(R.id.location);
        ImageView img = (ImageView) findViewById(R.id.image);
        Intent intent = getIntent();
        int dest_title = intent.getIntExtra("title", -9);
        int dest_info = intent.getIntExtra("info", -9);
        int dest_history = intent.getIntExtra("history", -9);
        String dest_location = intent.getStringExtra("location");
        if (intent.getBooleanExtra("hasImage", false)) {
            img.setImageResource(intent.getIntExtra("imageId", -1));
            img.setVisibility(View.VISIBLE);

        } else {
            img.setVisibility(View.GONE);
        }
        title.setText(dest_title);
        info.setText(dest_info);
        history.setText(dest_history);
        location.setText(dest_location);


    }
}
