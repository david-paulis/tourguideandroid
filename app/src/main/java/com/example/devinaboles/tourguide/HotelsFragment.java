package com.example.devinaboles.tourguide;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class HotelsFragment extends Fragment {


    public HotelsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        View rootView = inflater.inflate(R.layout.destinations_listview, container, false);
        final ArrayList<Destination> destinations = new ArrayList<>();

        destinations.add(new Destination(R.string.H_Grand_title, R.string.H_Grand_info, R.string.H_Grand_history, new Location("cairo", 1, "deniel st"), R.drawable.grand));
        destinations.add(new Destination(R.string.H_Marriott_title, R.string.H_Marriott_info, R.string.H_Marriott_history, new Location("cairo", 5, "gork st"), R.drawable.marriott));
        destinations.add(new Destination(R.string.H_Mena_House_title, R.string.H_Mena_House_info, R.string.H_Marriott_history, new Location("cairo", 2, "youue"), R.drawable.mena));
        destinations.add(new Destination(R.string.H_Fairmont_Nile_City_title, R.string.H_Fairmont_Nile_City_info, R.string.H_Fairmont_Nile_City_history, new Location("cairo", 8, "nour st"), R.drawable.fairmont));
        ListAdapter listAdapter = new ListAdapter(getActivity(), destinations);

        ListView listView = (ListView) rootView.findViewById(R.id.list);
        listView.setAdapter(listAdapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Destination destination = destinations.get(position);
                Intent intent = new Intent(getActivity(), DetailsActivity.class);
                intent.putExtra("title", destination.getTitle());
                intent.putExtra("info", destination.getInfo());
                intent.putExtra("history", destination.getHistory());
                intent.putExtra("location", destination.getLocation().toString());
                if (destination.hasImage()) {
                    intent.putExtra("hasImage", true);
                    intent.putExtra("imageId", destination.getImgResourceId());
                } else {
                    intent.putExtra("hasImage", false);
                }
                startActivity(intent);
            }
        });

        return rootView;
    }

}
